<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CubeSumationController extends Controller
{




    //
    public function cubeSumation(){
    	return view('cubesumation');
    }


    public function computeCubeSumation(){


		$totalCases = 0;
		$matrix; $updatedValues = array();
		$mSize = 0; $ops = 0;
		$i = 0;
		$startingTestCase = true;
		$executedQueries = 0;

    	$input = explode("\n", nl2br(htmlentities($_POST['input'])));
    	

    	foreach($input as $line){
    		
		    if ($i == 0){
		        $tCases = trim($line);
		        if ($tCases < 1 or $tCases > 50){
		        	break;
		        }
		    }elseif($startingTestCase){
		    	$this->getMatrixSizeAndTotalOps($line, $mSize, $ops);
		    	$startingTestCase = false;
		    	$matrix = $this->buildMatrix($mSize);
        		$updatedValues = array();

		    }else{
		        $executedQueries+=1; 

		        $this->excecuteQuery($line, $matrix, $updatedValues);
		        if($executedQueries == $ops){
		            $startingTestCase = true;
		            $executedQueries = 0;
		        }		    	
		    }
			
		    $i+=1;   	
    	}
    }


    function getMatrixSizeAndTotalOps($string, &$size, &$ops){
		$temp = explode(" ", trim($string));
	    $size = $temp[0];
	    $ops = $temp[1];  
	}

	function buildMatrix($size){
	    $matrix = array();
	    for ($i = 0; $i < $size; $i++) {
	        $matrix[$i] = array();
	        for ($j = 0; $j < $size; $j++) {
	            $matrix[$i][$j] = array();
	            for ($k = 0; $k < $size; $k++) {
	                $matrix[$i][$j][$k] = 0;
	            }
	        }
	    }
	    return $matrix;
	}

	function excecuteQuery($string, &$matrix, &$updatedValues){
	    $temp = explode(" ", trim($string));
	    if ($temp[0] == 'QUERY'){
	        //echo "Querying...\n";
	        $sum =  $this->query($temp[1], $temp[2], $temp[3], $temp[4], $temp[5], $temp[6], $updatedValues);
	        echo "$sum" . '<br />';
	    }else{
	        //echo "Updating...\n";
	        $this->update($temp[1], $temp[2], $temp[3], $temp[4], $matrix, $updatedValues);
	    }
	}

	function update($x, $y, $z, $newValue, &$matrix, &$updatedValues){
	    $matrix[$x-1][$y-1][$z-1] = $newValue;
	    
	    $this->addUpdatedValue($x, $y, $z, $newValue, $updatedValues);

	}

	function query($x1, $y1, $z1, $x2, $y2, $z2, &$updatedValues){
	    $sum = 0;
	    for($i = 0; $i < count($updatedValues); $i++){
	        $coordinate = $updatedValues[$i][0];
	        if ($coordinate[0] >= $x1 and $coordinate[0]<= $x2 and $coordinate[1] >= $y1 and $coordinate[1] <= $y2 and $coordinate[2] >= $z1 and $coordinate[2] <= $z2){
	            $sum += $updatedValues[$i][1];
	        }
	    }
	    return $sum;
	}

	function addUpdatedValue($x, $y, $z, $newValue, &$updatedValues){

	    $exist = false;
	    for($i = 0; $i < count($updatedValues); $i++){
	        
	        $coordinate = $updatedValues[$i][0];
	        if ($coordinate[0] == $x and $coordinate[1] == $y and $coordinate[2] == $z){
	            
	            $updatedValues[$i][1] = $newValue;
	            $exist = true;

	        }
	    }
	    
	    if (!$exist){
	        $value = array();
	        $value[0] = array($x, $y, $z); $value[1] = $newValue;

	        $updatedValues[] = $value;
	    }
	}

	function printUpdatedValues(&$updatedValues){
	    echo "Tamaño updatedvalues: " . count($updatedValues) . '<br />';
	    
	    for($i = 0; $i < count($updatedValues); $i++){
	        $coordinate = $updatedValues[$i][0];
	        echo $coordinate[0] . $coordinate[1] . $coordinate[2] . " - " . $updatedValues[$i][1] . " | ";
	    }
	    echo "\n";

	    
	}

}
