**Bakend Techonologies:** PHP

**Framework:** Laravel 5.3

**Requirements**: PHP >= 5.6.4
_____________________________________________________________

**INSTALLATION**

* Clone repository into desired folder.

```
#!console

git clone https://elindividuofrank@bitbucket.org/elindividuofrank/javier-franco-back.git
```

* From console, browse the directory that was cloned in step 1.

```
#!console

cd javier-franco-back
```

* Resolve and install dependencies required for the project from *composer.json*.

```
#!console

composer install
```

* Run a local server for the application

```
#!console

php artisan serve
```

* Point the browser to http://localhost:8000