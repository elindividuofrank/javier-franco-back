<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Cube Summation</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/jumbotron-narrow.css" rel="stylesheet">



  </head>

  <body>

    <div class="container">
      <div class="header clearfix">
        <nav>

        <h3 class="text-muted">Cube Summation</h3>
      </div>

      <div class="jumbotron">
        <form method="POST" action="/test">
            {!! csrf_field() !!}
            <textarea id="input" name="input" rows="10" cols="50">
            </textarea>
            <br>
            <input type="submit" value="Test">
        </form>
      </div>


      <footer class="footer">
        <p>Javier Franco</p>
      </footer>

    </div> <!-- /container -->

  </body>
</html>





