<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

/*
Route::get('/', function () {
	error_log('Some message here.');
    //return view('welcome');
    return view('cubesumation');
});
*/

Route::get('/', ['uses' => 'CubeSumationController@cubeSumation']);

Route::post('/test', ['uses' => 'CubeSumationController@computeCubeSumation']);
